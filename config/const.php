<?php
return [
    'del_flag' => array(
        'enable' => 0,
        'disable' => 1,
    ),
    'paging' => 8,
    'delete_on' => 1,
    'delete_off' => 0,
    'overview_type' => [
        0 => '表示',
        1 => 'タイトル',
        2 => '削除'
    ],
    'overview_type_text' => [
        'display' => '0',
        'title' => '1',
        'delete' => '2'
    ],
    'data_tag' => [
        'information_tag' => "<script type='text/javascript' src='%s'></script>",
        'over_view_tag' => "<script type='text/javascript' src='%s'></script>",
    ],
    'preview_url' => [
        'overview' => '/preview/overview/',
        'info' => '/preview/info/'
    ]
];
