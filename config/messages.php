<?php
return [
    'LOGIN_FAILED' => 'ID/PASS を確認してください',
    'TOKEN_TIMEOUT' => 'セッションの有効期限が切れています。 再度ログインを行なってください。',
    'SYSTEM_ERROR' => 'システムエラーが発生しました。',
    'DB_ERROR' => 'DB関連エラーが発生しました。',
    'HTTP401' => 'HTTP401認',
    'URL_INVALID' => 'URLが不正です',
    'NOT_FOUND' => 'URLが不正です',
    'PLEASE_LOG_IN' => '再度ログインしてください。',
    'NO_ORDER_DATA' => '注文発注データがありません。',
    'NO_DATA' => 'データがありません。',
    'ACCESS_DENIED' => '表示権限がありません。',

    'DB_INSERT' => '登録しました。',
    'DB_UPDATE' => '修正しました。',
    'DB_DELETE' => '削除しました。',
    'Server Error' => 'システムエラーが発生しました。',
    'stock' => array(
        'not_exist_product' => 'はご指定の受取り店舗または受取り日でのお受取りができない商品です。 カートから削除してください。',
        'stock_extra_start' => 'の店舗から、この商品のご入力いただいた数量の総計が在庫の数量を超えています。',
        'stock_extra_end' => '以下ご入力ください。',
        'stock_all_extra_start' => 'カートから、この商品のご入力いただいた数量の総計が在庫の数量を超えています。',
        'stock_all_extra_end' => '以下ご入力ください。',
        'stock_required' => 'この商品は在庫がございません。カートから削除してください。',
        'max_quantity_input' => 'カートに追加できる商品数の上限は999個までです。',
        'message_stock_block' => 'WEB予約は終了しました'
    ),
    'login' => array(
        'login_block' => 'このアカウントは現在ロック中です。時間をおいて再度アクセスください。',
    ),
    '500_error' => 'システムエラーが発生しました。',
    'bonus_message' => 'ポーナスポイント',

    'OLD_PASSWORD_INCORRECT' => '入力パスワードが間違いました、再度入力してください'
];