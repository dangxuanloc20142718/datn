<?php
return [
    'order' => [
        'sort_field' => [
            'slip_number',
            'status',
            'receptionist_date',
            'shop_code',
            'customer_name',
            'customer_tel'
        ]
    ],
    'event_index' => [
        'sort_field' => [
            'code',
            'name',
            'start_date',
            'end_date'
        ]
    ],
    'category_index' => [
        'sort_field' => [
            'code',
            'division_code',
            'name'
        ]
    ],
    'event_category' => [
        'sort_field' => [
            'event_code',
            'category_code',
            'sort'
        ]
    ],
    'event_view' => [
        'sort_field' => [
            'event_code',
            'sort'
        ]
    ],
    'event_item_view' => [
        'sort_field' => [
            'event_code',
            'category_code',
            'item_code',
            'sort',
            'receptionist_start_date',
            'receptionist_end_date',
            'receipt_start_date',
            'receipt_end_date'
        ]
    ],
    'item_index' => [
        'sort_field' => [
            'shop_code',
            'code',
            'content1'
        ]
    ],
    'item_name' => [
        'sort_field' => [
            'item_code',
            'name'
        ]
    ],
    'item_price' => [
        'sort_field' => [
            'item_code',
            'price'
        ]
    ],
    'item_point' => [
        'sort_field' => [
            'item_code',
            'receptionist_start_date',
            'receptionist_end_date',
            'receipt_start_date',
            'receipt_end_date',
            'point'
        ]
    ],
    'item_shop_count' => [
        'sort_field' => [
            'shop_code',
            'item_code',
            'receipt_date',
            'order_possible_count'
        ]
    ],
    'item_limited' => [
        'sort_field' => [
            'item_code',
            'order_possible_count'
        ]
    ],
    'item_list' => [
        'sort_field' => [
            'items_shop_code',
            'items_code',
            'count_reserve_item'
        ]
    ],
    'shop_index' => [
        'sort_field' => [
            'code',
            'status',
            'name',
            'name_kana',
            'area_id',
            'city_id',
            'open_time'
        ]
    ],
    'user_index' => [
        'sort_field' => [
            'id',
            'name',
            'card_no',
            'has_password',
            'email'
        ]
    ],
    'information_index' => [
        'sort_field' => [
            'id',
            'title',
            'public_start_date'
        ]
    ],
    'banner_index' => [
        'sort_field' => [
            'id',
            'banner_url',
            'public_start_date',
            'status',
            'link_url'
        ]
    ],
    'account_index' => [
        'sort_field' => [
            'id',
            'name',
            'status',
            'login_id'
        ]
    ]
];