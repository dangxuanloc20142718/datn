<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class AdministratorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('administrators')->truncate();

        // Create virtual DB
        $faker = Faker::create();
        $admin = [];

        // Super admin
        $admin[] = [
            'login_id' => 'admin',
            'password' => \Illuminate\Support\Facades\Hash::make('admin123'),
            'del_flag' => '0',
            'ins_id' => 1,
            'ins_datetime' => Carbon::now()->format('Y-m-d H:i:s'),
        ];
        for ($i = 0; $i < 20; $i++) {
            $admin[] = [
                'login_id' => $faker->userName,
                'password' => $faker->password,
                'del_flag' => '0',
                'ins_id' => 1,
                'ins_date' => Carbon::now()->format('Y-m-d H:i:s'),
            ];
        }

        // Insert DB
        DB::table('administrators')->insert($admin);
    }
}
