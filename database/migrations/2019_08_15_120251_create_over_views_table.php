<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverViewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overviews', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->unsignedInteger('clients_id')->length('clients_id');
            $table->char('type', 1)->comment('区分 | 0：表示、1：タイトル、2：削除');
            $table->string('title', 256)->comment('タイトル');
            $table->text('content')->comment('内容');
            $table->smallInteger('sort')->comment('表示順');
            $table->char('del_flag', 1)->default('0')->comment('削除フラグ | 0：未削除、1：削除');
            $table->dateTime('ins_datetime')->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->nullable()->default('0000-00-00 00:00:00')->comment('更新日時');
            $table->integer('upd_id')->nullable()->comment('更新者ID');

            //set index attribute
            $table->index('id');
            $table->index('del_flag');

            //foreign key

            $table->foreign('clients_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overviews');
    }
}
