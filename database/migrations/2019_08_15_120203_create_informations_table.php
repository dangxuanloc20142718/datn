<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('informations', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->unsignedInteger('clients_id')->length(11)->comment('クライアントID');
            $table->longText('html_content')->nullable()->comment('HTMLコンテンツ');
            $table->char('del_flag', 1)->default('0')->comment('削除フラグ | 0：未削除、1：削除');
            $table->dateTime('ins_datetime')->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->nullable()->default('0000-00-00 00:00:00')->comment('更新日時');
            $table->integer('upd_id')->nullable()->comment('更新者ID');

            //set index attribute
            $table->index('id');
            $table->index('del_flag');

            //foreign key
            $table->foreign('clients_id')->references('id')->on('clients');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('informations');
    }
}
