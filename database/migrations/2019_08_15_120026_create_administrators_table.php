<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdministratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrators', function (Blueprint $table) {
            $table->increments('id')->length(11);
            $table->string('login_id', 256)->comment('ログインID');
            $table->char('password', 128)->comment('パスワード');
            $table->char('del_flag', 1)->default('0')->comment('削除フラグ | 0：未削除、1：削除');
            $table->dateTime('ins_datetime')->comment('登録日時');
            $table->integer('ins_id')->length(11)->comment('登録者ID');
            $table->dateTime('upd_datetime')->nullable()->default('0000-00-00 00:00:00')->comment('更新日時');
            $table->integer('upd_id')->nullable()->comment('更新者ID');

            //set index attribute
            $table->index('id');
            $table->index('del_flag');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrators');
    }
}
