<?php

use Illuminate\Support\Facades\Route;

Route::get('/', 'ClientController@getLogin')->name('client.login');
Route::post('/post/login', 'ClientController@postLogin')->name('client.post.login');
Route::get('/register','ClientController@register')->name('client.register');
Route::post('/logout', 'ClientController@logout')->name('client.logout');
//Route::group(['middleware' => 'client.authenticate'], function () {
    Route::get('/home', 'ClientController@index')->name('client.home');
    Route::get('/search', 'ClientController@search')->name('client.search');
    Route::get('/contact', 'ClientController@contact')->name('client.contact');
    Route::get('/post', 'ClientController@post')->name('client.post');
//});