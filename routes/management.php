<?php

use Illuminate\Support\Facades\Route;


Route::get('/login', 'ManagementController@getLogin')->name('management.login');
Route::post('/login', 'ManagementController@postLogin')->name('management.post.login');
Route::post('/logout', 'ManagementController@logout')->name('management.logout');
Route::group(['middleware' => 'management.authenticate'], function () {
    Route::get('/', 'ManagementController@index')->name('management.home');
    Route::get('/detail/{id}', 'ManagementController@detail')->name('management.detail');
    Route::get('/password', 'ManagementController@changePassword')->name('management.password');
    Route::post('/password/update', 'ManagementController@updatePassword')->name('management.password.update');
    Route::get('/edit/account/{id}', 'ManagementController@edit')->name('management.edit.account');
    Route::post('edit/account/{id}','ManagementController@update')->name('management.update.account');
    Route::get('/edit/info/{id}', 'ManagementController@editInfo')->name('management.edit.info');
    Route::post('/edit/info/{id}', 'ManagementController@postEditInfo')->name('management.post.edit.info');
    Route::post('/edit/overview/{id}', 'ManagementController@postEditOverview')->name('management.post.edit.overview');
    Route::get('/add', 'ManagementController@addClient')->name('management.add.client');
    Route::post('/add', 'ManagementController@postAddClient')->name('management.post.add.client');
    Route::post('/confirm', 'ManagementController@confirmClient')->name('management.confirm');
    Route::get('/delete/{idClient}', 'ManagementController@deleteClient')->name('management.delete.client');
});