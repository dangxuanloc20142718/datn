<div class="inner clearfix">
    <h1><img src="{{asset('common/img/sitelogo01.svg')}}" alt="Index"/></h1>
    <table border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>ログインID：{{Auth::guard('post')->user()->login_id}}</td>
            <td class="toggleMenuIco hover"><a href="javascript:void(0);"><img
                            src="{{asset('common/img/ico_hdMenu01.png')}}" width="30" height="30" alt="メニュー"/></a></td>
        </tr>
    </table>
    <div class="toggleMenu">
        <form method="POST" name="fm_logout" id="fm_login" action="{{route('management.logout')}}">
            @csrf
            <ul>
                <li><a href="{{route('management.home')}}">クライアント一覧</a></li>
                <li><a href="{{route('management.password')}}">パスワード変更</a></li>
                <li>
                    <input type="submit" value="ログアウト"/>
                    <input type="hidden" name="submitLogout" value="1">
                </li>
            </ul>
        </form>
    </div>
</div>