<div class="w3-col m3 padding-minus-15px">
    <!-- Profile -->
    <div class="w3-card w3-round w3-white">
        <div class="w3-container">
            <h4 class="w3-center">My Profile</h4>
            <p class="w3-center"><img src="/w3images/avatar3.png" class="w3-circle" style="height:106px;width:106px" alt="Avatar"></p>
            <hr>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Tỉnh/Thành phố đi</label>
                    <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Tỉnh/Thành phố đi</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Huyện/Quận đi</label>
                    <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Huyện/Quận đi</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Tỉnh/Thành phố đến</label>
                    <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Tỉnh/Thành phố đến</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                    </select>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label>Huyện/Quận đến</label>
                    <select class="form-control select2" style="width: 100%;">
                        <option selected="selected">Huyện/Quận đến</option>
                        <option>Alaska</option>
                        <option>California</option>
                        <option>Delaware</option>
                        <option>Tennessee</option>
                        <option>Texas</option>
                        <option>Washington</option>
                    </select>
                </div>
            </div>
            <br/>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="type">Hình thức</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-bullseye"></i></span>
                        <select class="form-control" onchange="PostsController.displaySchedule(this)" id="type" name="type"><option value="1" selected="selected">Xe tìm khách</option><option value="2">Khách tìm xe</option></select>
                    </div>
                </div>
            </div>
            <p><i class="fa fa-home fa-fw w3-margin-right w3-text-theme"></i> London, UK</p>
            <p><i class="fa fa-birthday-cake fa-fw w3-margin-right w3-text-theme"></i> April 1, 1988</p>
        </div>
    </div>
    <br>
    <!-- Accordion -->
    <div class="w3-card w3-round">
        <div class="w3-white">
            <button onclick="myFunction('Demo1')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-circle-o-notch fa-fw w3-margin-right"></i> My Groups</button>
            <div id="Demo1" class="w3-hide w3-container">
                <p>Some text..</p>
            </div>
            <button onclick="myFunction('Demo2')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-calendar-check-o fa-fw w3-margin-right"></i> My Events</button>
            <div id="Demo2" class="w3-hide w3-container">
                <p>Some other text..</p>
            </div>
            <button onclick="myFunction('Demo3')" class="w3-button w3-block w3-theme-l1 w3-left-align"><i class="fa fa-users fa-fw w3-margin-right"></i> My Photos</button>
            <div id="Demo3" class="w3-hide w3-container">
                <div class="w3-row-padding">
                    <br>
                    <div class="w3-half">
                        <img src="/w3images/lights.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                    <div class="w3-half">
                        <img src="/w3images/nature.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                    <div class="w3-half">
                        <img src="/w3images/mountains.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                    <div class="w3-half">
                        <img src="/w3images/forest.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                    <div class="w3-half">
                        <img src="/w3images/nature.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                    <div class="w3-half">
                        <img src="/w3images/snow.jpg" style="width:100%" class="w3-margin-bottom">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>