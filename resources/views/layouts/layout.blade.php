<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
@include('elements.meta')
<style>
    html, body, h1, h2, h3, h4, h5 {font-family: "Open Sans", sans-serif}
</style>
<body class="w3-theme-l5">

<!-- Navbar -->
@include('elements.header_client')

<!-- Navbar on small screens -->
<div id="navDemo" class="w3-bar-block w3-theme-d2 w3-hide w3-hide-large w3-hide-medium w3-large">
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Trang chủ</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Trang chủ</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Tin tức</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Đăng bài</a>
    <a href="#" class="w3-bar-item w3-button w3-padding-large">Đăng nhập</a>
</div>

<!-- Page Container -->
<div class="w3-container w3-content" style="max-width:1400px;margin-top:80px">
    <!-- The Grid -->
    <div class="w3-row">
        <!-- Left Column -->
        @include('elements.client_sidebar_search')

        <!-- Middle Column -->
        @yield('content')
    </div>

    <!-- End Page Container -->
</div>
<br>

<!-- Footer -->
@include('elements.footer')

<script>
    // Accordion
    function myFunction(id) {
        var x = document.getElementById(id);
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
            x.previousElementSibling.className += " w3-theme-d1";
        } else {
            x.className = x.className.replace("w3-show", "");
            x.previousElementSibling.className =
                x.previousElementSibling.className.replace(" w3-theme-d1", "");
        }
    }

    // Used to toggle the menu on smaller screens when clicking on the menu button
    function openNav() {
        var x = document.getElementById("navDemo");
        if (x.className.indexOf("w3-show") == -1) {
            x.className += " w3-show";
        } else {
            x.className = x.className.replace(" w3-show", "");
        }
    }
</script>

</body>
</html>
