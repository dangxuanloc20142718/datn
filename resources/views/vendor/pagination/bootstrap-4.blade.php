@if($paginator->hasPages())
<div class="calendar">
    <ul class="pager">
        <li class="over">
            @if ($paginator->onFirstPage())
                <input type="button" aria-disabled="true" class="previous" id="previous" value="＜"
                >
            @else
                <input type="button" aria-disabled="true" class="previous" id="previous" value="＜"
                       onclick="deincrease('{{$paginator->previousPageUrl()}}')">
        @endif

        @foreach ($elements as $element)
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <input type="text" id="page"
                               onchange="nowPage('{{$url}}','{{$page}}','{{count($element)}}')" name="page"
                               value="{{$paginator->currentPage()}}" style="width:1.3em;text-align: center">
                        / <span id="total">{{$paginator->lastPage()}}</span>
                    @endif
                @endforeach
            @endif
        @endforeach


        @if ($paginator->currentPage() == $paginator->lastPage())
            <input type="button" aria-disabled="true" class="next " id="next" value="＞"
            >
        @else
            <input type="button" class="next" id="next" value="＞"
                   onclick="increase('{{ $paginator->nextPageUrl()}}')">
        @endif
    </ul>
</div>
@endif
