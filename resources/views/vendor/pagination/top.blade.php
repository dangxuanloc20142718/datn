@if ($paginator->hasPages())
    <div class="col-xs-12 col-sm-12 col-lg-12 pull-right pagination">
        <ul class="pagination">
            @if ($paginator->onFirstPage())
                <li class="">
                    <span><i class="fas fa-chevron-left"></i></span>
                </li>
            @else
                <li class="">
                    <a href="{{ $paginator->onFirstPage() }}">
                        <span><i class="fas fa-chevron-left"></i><i class="fas fa-chevron-left"></i></span>
                    </a>
                </li>

                <li>
                    <a href="{{ $paginator->previousPageUrl() }}">
                        <span><i class="fas fa-chevron-left"></i></span>
                    </a>
                </li>
            @endif
            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- Array Of Links --}}
                @if (is_array($element) && !empty($paginator->total()))
                    @foreach ($element as $page => $url)
                        <!--  Show active page else show the first and last two pages from current page.  -->
                            @if ($page == $paginator->currentPage())
                                <li class="active"><a href="{{ $url }}">{{ $page }}</a></li>
                            @elseif($page < $paginator->currentPage()-3)
                            @elseif ($page < $paginator->currentPage() +6 || $page === $paginator->currentPage() - 1 || $page === $paginator->currentPage() - 2 || $page === $paginator->lastPage() || $page === 1)
                                <li><a href="{{ $url }}">{{ $page }}</a></li>
                            @endif

                                <!--  Use three dots when current page is away from end.  -->
                            @if ($paginator->currentPage() < $paginator->lastPage() - 3 && $page === $paginator->lastPage() - 1 && $paginator->currentPage() < $paginator->lastPage()-5)
                                <div><span>..</span></div>
                            @endif
                    @endforeach
                @endif
            @endforeach
            @if ($paginator->hasMorePages())
                <li>
                    <a href="{{ $paginator->nextPageUrl() }}">
                        <span><i class="fas fa-chevron-right"></i></span>
                    </a>
                </li>
            @else
                <li class="disabled">
                    <span><i class="fas fa-chevron-right"></i></span>
                </li>
            @endif
        </ul>
    </div>
@endif