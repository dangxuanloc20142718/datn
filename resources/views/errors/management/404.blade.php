@extends('layouts.layout')
@section('content')
    <div id="preview_error">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <div>URLが不正です</div>
        </div>
    </div>
    <a href="{{route('management.home')}}"><img src="{{asset('common/img/btn_goBack.png')}}" alt="戻る"></a>
@endsection