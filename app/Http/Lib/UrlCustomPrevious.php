<?php
namespace App\Http\Lib;
use Illuminate\Http\Request;
use Illuminate\Routing\UrlGenerator;

/**
 * Class UrlCustomPrevious
 * @package App\Http\Lib
 */
class UrlCustomPrevious extends UrlGenerator
{
    protected $request;

    public function __construct(UrlGenerator $url)
    {
        parent::__construct($url->routes, $url->request);
    }

    public function previous($fallback = false)
    {
        $referrer = $this->request->headers->get('referer');

        $url = $referrer ? $this->to($referrer) : $this->getPreviousUrlFromSession();
        $previousHost = parse_url($url);
        if (!empty($previousHost['scheme']) && !empty($previousHost['host'])) {
            $previousHost = $previousHost['scheme'] . '://' . $previousHost['host'];
            if ($this->request->getSchemeAndHttpHost() == $previousHost) {
                if ($url) {
                    return $url;
                } elseif ($fallback) {
                    return $this->to($fallback);
                }
            }
        }
        return $this->to('/');
    }
}