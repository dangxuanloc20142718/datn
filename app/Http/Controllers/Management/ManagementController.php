<?php

namespace App\Http\Controllers\Management;

use App\Http\Requests\EditOverviewRequest;
use App\Repositories\ClientsRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CreateClientRequest;
use App\Http\Requests\EditClientRequest;
use App\Http\Requests\UpdatePasswordAdministratorRequest;
use App\Repositories\AdministratorsRepository;
use App\Repositories\InformationRepository;
use App\Repositories\OverviewRepository;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class ManagementController extends Controller
{
    protected $administratorRepository;
    protected $clientRepository;
    protected $informationRepository;
    protected $overviewRepository;

    public function __construct(ClientsRepository $clientRepository, InformationRepository $informationRepository, OverviewRepository $overviewRepository, AdministratorsRepository $administratorRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->administratorRepository = $administratorRepository;
        $this->clientRepository = $clientRepository;
        $this->informationRepository = $informationRepository;
        $this->overviewRepository = $overviewRepository;
    }

    public function guard()
    {
        return Auth::guard('management');
    }

    public function getLogin()
    {

        if ($this->guard()->check()) {
            return redirect()->route('management.home');
        } else {
            return view('management.login.login');
        }

    }

    public function postLogin(Request $request)
    {
        $info = $request->only(['login_id', 'password']);
        if ($this->guard()->attempt($info)) {
            return redirect()->route('management.home');
        }
        $request->flash();
        return redirect()->route('management.login')->withErrors(config('messages.LOGIN_FAILED'));

    }

    public function logout()
    {
        $this->guard()->logout();
        return redirect()->route('management.login');
    }

    public function index(Request $request)
    {
        $loginID = $request->has('login_id_search') ? $request->get('login_id_search') : null;
        $listClient = $this->administratorRepository->getListClient($loginID);
        $request->flash();
        return view('management.account.account', compact('listClient'));
    }

    public function detail($id)
    {
        $client = $this->clientRepository->find($id);
        $data_client_info = $this->informationRepository->getFirstDataByCondition(['clients_id' => $id]);
        $data_client_overview = $this->overviewRepository->getAllDataByCondition(['clients_id' => $id]);
        return view('management.account.account_detail', compact('data_client_info', 'data_client_overview', 'client', 'id'));
    }

    public function changePassword()
    {
        $idManagement = $this->guard()->id();
        $dataManagement = $this->administratorRepository->find($idManagement);
        return view('management.account.password', compact('dataManagement'));
    }

    public function updatePassword(UpdatePasswordAdministratorRequest $request)
    {
        $oldPassword = $request->old_password;
        $newPassword = Hash::make($request->new_password);
        $idUser = $this->guard()->user()->id;
        if ($this->administratorRepository->checkPassword($oldPassword)) {
            try {
                $this->administratorRepository->update(['password' => $newPassword], $idUser, $idUser);
                return redirect()->route('management.home')->with('success', config('messages.DB_UPDATE'));
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return redirect()->route('management.password')->with('error', config('messages.SYSTEM_ERROR'));
            }
        } else {
            $request->flash();
            return redirect()->route('management.password')->with('error', config('messages.OLD_PASSWORD_INCORRECT'));
        }
    }

    public function edit($id)
    {
        $client = $this->clientRepository->find($id);
        return view('management.account.account_edit', compact('client'));
    }

    public function update(EditClientRequest $request, $id)
    {
        $id_user = $this->guard()->user()->id;
        $infoChange = $request->only('name', 'login_id', 'comment');
        if (!empty($request->new_password)) {
            $infoChange['password'] = Hash::make($request->new_password);
        }
        try {
            $this->clientRepository->update($infoChange, $id, $id_user);
            return redirect()->route('management.detail', $id)->with('success', config('messages.DB_UPDATE'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return redirect()->route('management.detail', $id)->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    public function editInfo($id)
    {
        $dataClient = $this->clientRepository->find($id);
        $information = $this->clientRepository->getInformation($id);
        $listOverview = $this->clientRepository->getOverviews($id);
        return view('management.account.account_edit_he', compact('id', 'information', 'listOverview', 'dataClient'));
    }

    public function postEditInfo(Request $request, $id)
    {
        $data = $request->only('html_content');
        $idUser = $this->guard()->user()->id;
        $checkInfo = $this->informationRepository->checkInfo($id);
        if ($checkInfo) {//update
            try {
                $idInformation = $this->informationRepository->getIDInformation($id);
                $this->informationRepository->update($data, $idInformation, $id);
                $this->clientRepository->updateInformationUrl($data, $idInformation, $id, $idUser);
                return redirect()->route('management.detail', $id);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return redirect()->route('management.detail', $id);
            }

        } else {//create
            try {
                $data['clients_id'] = $id;
                $information_id = $this->informationRepository->create($data);
                $this->clientRepository->updateInformationUrl($data, $information_id, $id, $idUser);
                return redirect()->route('management.detail', $id);
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return redirect()->route('management.detail', $id);
            }
        }

    }

    public function postEditOverview(EditOverviewRequest $request, $id)
    {
        $userId = $this->guard()->id();
        $dataUpdate = $dataInsert = $dataDelete = array();
        $dataOverviewId = $this->overviewRepository->findDataClientId($id);
        $dataRequest = $this->overviewRepository->processRequest($request->all());
        $keyUpdate = array_keys($dataOverviewId);
        DB::beginTransaction();
        if (empty($dataRequest)) {
            return redirect()->route('management.detail', ['id' => $id, 'overview'])->with('success', config('messages.DB_UPDATE'));
        }
        try {
            foreach ($dataRequest['overview_title'] as $key => $value) {
                if (in_array($key, $keyUpdate)) {
                    $dataUpdate = [
                        'title' => $value,
                        'content' => $dataRequest['overview_content'][$key],
                        'type' => $dataRequest['overview_type'][$key],
                    ];
                    $this->overviewRepository->updateOverview($dataUpdate, $dataOverviewId[$key], $userId);
                    unset($dataOverviewId[$key]);
                    unset($keyUpdate[$key]);
                } elseif (!in_array($key, $keyUpdate)) {
                    $dataInsert[] = [
                        'title' => $value,
                        'clients_id' => $id,
                        'content' => $dataRequest['overview_content'][$key],
                        'type' => $dataRequest['overview_type'][$key],
                        'ins_datetime' => Carbon::now()
                    ];
                    unset($dataOverviewId[$key]);
                }
            }
            if (!empty($dataInsert)) {
                $this->overviewRepository->insertMulti($dataInsert);
            }
            if (!empty($dataOverviewId)) {
                $this->overviewRepository->deleteMulti($dataOverviewId);
            }
            $data_content = [
                'other_content' => $request->get('other_content')
            ];
            $this->clientRepository->updateOtherContent($data_content, $id, $userId);
            $this->overviewRepository->insertOverviewUrl($id, $userId);
            DB::commit();
            return redirect()->route('management.detail', ['id' => $id, 'overview']);
        } catch (\Exception $e) {
            DB::rollBack();
            Log::warning('UPDATE_ERROR', ['CONTENT' => $e->getMessage()]);
            return redirect()->route('management.detail', ['id' => $id, 'overview']);
        }
    }

    public function addClient()
    {
        return view('management.account.account_new');
    }

    public function confirmClient(CreateClientRequest $request)
    {
        $request->flash();
        $clientInfo = $request->all();
        Session::put('clientInfo', $clientInfo);
        return view('management.account.account_new_confirm', compact('clientInfo'));
    }

    public function postAddClient()
    {
        if (Session::has('clientInfo')) {
            $clientInfo = Session::get('clientInfo');
            $clientInfo ['password'] = Hash::make($clientInfo['password']);
            $userId = $this->guard()->id();
            try {
                $idClient = $this->clientRepository->create($clientInfo);
                $data_tag = [
                    'information_tag' => $idClient,
                    'overview_tag' => $idClient
                ];
                $this->clientRepository->update($data_tag, $idClient, $userId);
                Session::forget('clientInfo');
                return redirect()->route('management.home')->with('success', config('messages.DB_INSERT'));
            } catch (\Exception $e) {
                Log::info($e->getMessage());
                return redirect()->route('management.home')->with('error', config('messages.SYSTEM_ERROR'));
            }
        } else {
            return redirect()->route('management.home')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }

    public function deleteClient($idClient)
    {
        $idManagement = $this->guard()->user()->id;
        try {
            $this->clientRepository->softDelete($idClient, $idManagement);
            return redirect()->route('management.home')->with('success', config('messages.DB_DELETE'));
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return redirect()->route('management.home')->with('error', config('messages.SYSTEM_ERROR'));
        }
    }
}
