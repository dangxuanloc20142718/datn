<?php

namespace App\Http\Controllers\Client;

use App\Http\Requests\EditOverviewRequest;
use App\Http\Requests\UpdatePasswordClientRequest;
use App\Repositories\ClientsRepository;
use App\Repositories\InformationRepository;
use App\Repositories\OverviewRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\LoginClientRequest;
use Illuminate\Support\Facades\Hash;
use Mockery\Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;

class ClientController extends Controller
{
    protected $clientRepository;
    protected $informationRepository;
    protected $overviewRepository;

    public function __construct(ClientsRepository $clientRepository, InformationRepository $informationRepository, OverviewRepository $overviewRepository)
    {
        $this->clientRepository = $clientRepository;
        $this->informationRepository = $informationRepository;
        $this->overviewRepository = $overviewRepository;
    }

    public function guard()
    {
        return Auth::guard('client');
    }

    public function getLogin()
    {
        return view('client.account.login');
    }

    public function postLogin(Request $request)
    {
        $info = $request->only(['login_id', 'password']);

        if ($this->guard()->attempt($info)) {
            return redirect()->route('client.home');
        }
        $request->flash();
        return redirect()->route('client.login')->withErrors(config('messages.LOGIN_FAILED'));
    }

    public function register(){
        return view('client.account.register');
    }

    public function logout()
    {
        $this->guard()->logout();
        return redirect()->route('client.login');
    }

    public function index()
    {
        return view('client.home.index');
    }

    public function password()
    {
        $idClient = $this->guard()->id();
        $dataClient = $this->clientRepository->find($idClient);
        return view('client.account.password', compact('dataClient'));
    }

    public function search(Request $request)
    {
        return view('client.news.index');
    }

    public function contact()
    {
      return view('client.contact.index');
    }

}
