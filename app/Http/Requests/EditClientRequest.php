<?php

namespace App\Http\Requests;

use App\Models\Clients;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Factory as ValidationFactory;

class EditClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function __construct(ValidationFactory $validationFactory)
    {
        request()->flash();
        $validationFactory->extend(
            'check_new_password',
            function () {
                $newPassword = request()->get('new_password');
                if(empty($newPassword) || strlen($newPassword) >=8 && strlen($newPassword) <=20){
                    return true;
                }
                   return false;
            },"PASSは8～20文字の半角英字の大文字、小文字、<br />数字を必ず1文字は含めて入力してください。"
        );
        return false;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule =  [
            'name' => 'required',
            'login_id' => ['required', 'max:32',
                Rule::unique('clients')->where('del_flag','0')->ignore($this->request->get('id'), 'id')
            ],
            'new_password' => 'check_new_password',
            'confirm_password' => 'same:new_password'
        ];
        $this->redirect = route('management.edit.account',[request()->id,'flag'=>1]);
        return $rule;
    }
}
