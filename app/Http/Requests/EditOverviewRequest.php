<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Factory as ValidationFactory;

class EditOverviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function __construct(ValidationFactory $validationFactory)
    {
        request()->flash();
        $validationFactory->extend(
            'check_data',
            function ($attribute, $value, $parameters) {
                $data_content = request()->get('overview_content');
                $data_title = request()->get('overview_title');
                if (!empty($data_title)) {
                    foreach ($data_title as $key => $value) {
                        if ($value == null && $data_content[$key] != null) {
                            return false;
                        }
                    }
                }
                return true;
            }, 'タイトルが必要です、入力してください。'
        );
        return false;
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [
            'overview_title' => 'check_data',
//            'overview_create' =>'sometimes|check_data'
        ];
        if (strlen(strstr(url()->current(), 'management')) > 0) {
            $this->redirect = route('management.edit.info', ['id' => request()->get('client_id'), 'overview', 'flag']);
        }
        if (strlen(strstr(url()->current(), 'client')) > 0) {
            $this->redirect = route('client.info.edit', ['overview', 'flag']);
        }
        return $rule;
    }
}
