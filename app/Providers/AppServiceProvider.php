<?php

namespace App\Providers;

use App\Http\Lib\UrlCustomPrevious;
use App\Models\Reserve;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        $url = $this->app['url'];
        $this->app->singleton('url', function () use ($url) {
            return new UrlCustomPrevious($url);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }

}
