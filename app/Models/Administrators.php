<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Administrators extends Model implements Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }

    protected $table = 'administrators';
    protected $fillable = [
        'login_id',
        'password',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];
    protected $hidden = ['password'];
    public $timestamps = false;

}
