<?php

namespace App\Models;

use App\Scopes\ActiveScope;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Clients extends Model implements Authenticatable
{
    use Notifiable;
    use AuthenticableTrait;

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }

    protected $table = 'clients';
    protected $fillable = [
        'name',
        'login_id',
        'password',
        'other_content',
        'information_tag',
        'information_preview_url',
        'overview_tag',
        'overview_preview_url',
        'comment',
        'del_flag',
        'ins_datetime',
        'ins_id',
        'upd_datetime',
        'upd_id'
    ];
    protected $hidden = ['password'];
    public $timestamps = false;

    public function getInformation()
    {
        return $this->hasOne('App\Models\Informations', 'clients_id', 'id');
    }

    public function getOverview()
    {
        return $this->hasMany('App\Models\Overviews', 'clients_id', 'id');
    }
}
