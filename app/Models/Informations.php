<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Informations extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }

    protected $table = 'informations';
    protected $fillable =
        [
            'clients_id',
            'html_content',
            'del_flag',
            'ins_datetime',
            'ins_id',
            'upd_datetime',
            'upd_id'
        ];
    public $timestamps = false;
}
