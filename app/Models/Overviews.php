<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Scopes\ActiveScope;

class Overviews extends Model
{
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new ActiveScope());
    }

    protected $table = 'overviews';
    protected $fillable =
        [
            'clients_id',
            'type',
            'title',
            'content',
            'sort',
            'del_flag',
            'ins_datetime',
            'ins_id',
            'upd_datetime',
            'upd_id'
        ];
    public $timestamps = false;
}
