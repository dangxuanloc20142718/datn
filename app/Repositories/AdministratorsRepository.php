<?php
/**
 * Created by PhpStorm.
 * User: DELL
 * Date: 8/16/2019
 * Time: 1:17 PM
 */

namespace App\Repositories;


use App\Models\Administrators;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AdministratorsRepository extends BaseRepository
{
    protected $clientRepository;

    public function __construct(ClientsRepository $clientRepository)
    {
        $this->model = Administrators::class;
        $this->clientRepository = $clientRepository;
    }

    public function checkPassword($oldPassword)
    {
        if ((Hash::check($oldPassword, Auth::guard('management')->user()->password))) {
            return true;
        }
        return false;
    }

    public function getListClient($loginID)
    {
        return $this->clientRepository->getModel()
            ->when($loginID != null, function ($query) use ($loginID) {
                $query->where('login_id', 'like', '%' . $loginID . '%')->orWhere('name', 'like', '%' . $loginID . '%');
            })
            ->orderBy('ins_datetime', 'DESC')
            ->paginate(config('const.paging'));
    }
}