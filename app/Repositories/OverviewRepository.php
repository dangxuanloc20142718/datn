<?php

namespace App\Repositories;

use App\Models\Clients;
use App\Models\Overviews;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class OverviewRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = Overviews::class;
    }

    public function updateOverview($dataUpdate, $id, $user_id)
    {
        $this->update($dataUpdate, $id, $user_id);
    }

    public function findDataClientId($client_id)
    {
        $data_result = [];
        $data = Overviews::where('clients_id', $client_id)->get();
        if (!$data->isEmpty()) {
            foreach ($data as $value) {
                array_push($data_result, $value->id);
            }
        }
        return $data_result;
    }

    public function processRequest($data)
    {
        $data_result = [];
        foreach ($data['overview_title'] as $key => $value) {
            if ($value == null && $data['overview_content'][$key] == null || $data['overview_type'][$key] == config('const.overview_type_text.delete')) {
                unset($data['overview_title'][$key]);
                unset($data['overview_content'][$key]);
                unset($data['overview_type'][$key]);
            }
        }
        $data_result['overview_title'] = array_values($data['overview_title']);
        $data_result['overview_content'] = array_values($data['overview_content']);
        $data_result['overview_type'] = array_values($data['overview_type']);
        return $data_result;
    }

    public function insertOverviewUrl($client_id, $user_id)
    {
        $data_update = [
            'upd_id' => $user_id,
            'upd_datetime' => Carbon::now()
        ];
        $data_overview = $this->getAllDataByCondition(['clients_id' => $client_id]);
        if (!$data_overview->isEmpty()) {
            $data_update['overview_preview_url'] = config('const.preview_url.overview') . $data_overview[0]->id;
        } else {
            $data_update['overview_preview_url'] = "";
        }
        Clients::where('id', $client_id)->update($data_update);
    }
}