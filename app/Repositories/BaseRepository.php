<?php

namespace App\Repositories;

use Carbon\Carbon;

class BaseRepository
{
    protected $model;

    public function getModel()
    {
        return app()->make($this->model);
    }

    public function all()
    {
        return $this->getModel()->all();
    }

    public function create($data)
    {
        $data['ins_datetime'] = Carbon::now();
        $object = new $this->model($data);
        $object->save();
        return $object->id;
    }

    public function update($data, $id, $user_id)
    {
        $data['upd_datetime'] = Carbon::now();
        $data['upd_id'] = $user_id;
        $object = $this->model::find($id);
        $object->update($data);
        return true;
    }

    public function softDelete($id, $user_id)
    {
        $data = [
            'del_flag' => config('const.delete_on')
        ];
        $this->update($data, $id, $user_id);
    }

    public function find($id)
    {
        return $this->getModel()->findOrFail($id);
    }

    public function deleteMulti($listId)
    {
        $dataUpdate = [
            'del_flag' => config('const.delete_on')
        ];
        $object = $this->model::whereIn('id', $listId)->update($dataUpdate);
        return true;
    }

    public function insertMulti($dataInsert)
    {
        $object = $this->model::insert($dataInsert);
        return true;
    }

    public function getAllDataByCondition($condition = array(), $paginate = null)
    {
        $data = $this->getModel();
        if (!empty($condition)) {
            foreach ($condition as $key => $value) {
                $data = $data->where($key, $value);
            }
        }
        if (!empty($paginate)) {
            return $data->paginate($paginate);
        } else {
            return $data->get();
        }
    }

    public function getFirstDataByCondition($condition = array())
    {
        $data = $this->getModel();
        if (!empty($condition)) {
            foreach ($condition as $key => $value) {
                $data = $data->where($key, $value);
            }
        }
        return $data->first();
    }
}