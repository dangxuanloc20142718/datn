<?php

namespace App\Repositories;

use App\Models\Clients;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class  ClientsRepository extends BaseRepository
{
    protected $overviewRepository;

    public function __construct(OverviewRepository $overviewRepository)
    {
        $this->model = Clients::class;
        $this->overviewRepository = $overviewRepository;
    }

    public function checkPassword($oldPassword)
    {
        if ((Hash::check($oldPassword, Auth::guard('client')->user()->password))) {
            return true;
        }
        return false;
    }

    public function getInformation($id)
    {
        $client = $this->find($id);
        return $client->getInformation;
    }

    public function getOverviews($id)
    {
        $client = $this->find($id);
        return $client->getOverview;
    }

    public function getListOverview($id)
    {
        $client = $this->overviewRepository->find($id);
        return $client;
    }

    public function updateOtherContent($data, $id, $user_id)
    {
        $this->update($data, $id, $user_id);
    }

    public function updateInformationUrl($data_content, $id_information, $id_client, $id_user)
    {
        $data_update['information_preview_url'] = "";
        if ($data_content['html_content'] != null) {
            $data_update['information_preview_url'] = config('const.preview_url.info') . $id_information;
        }
        $this->update($data_update, $id_client, $id_user);
    }
}