<?php

namespace App\Repositories;

use App\Models\Informations;

class InformationRepository extends BaseRepository
{
    public function __construct()
    {
        $this->model = Informations::class;
    }

    public function checkInfo($idClient)
    {
        $checkInfo = $this->getModel()->where('clients_id', $idClient)->first();
        if ($checkInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    public function getIDInformation($idClient)
    {
        return $this->getModel()->where('clients_id', $idClient)->first()->id;
    }
}