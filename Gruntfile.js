"use strict";

module.exports = function (grunt) {
    // Load grunt tasks automatically
    require("load-grunt-tasks")(grunt);
    grunt.loadNpmTasks("grunt-contrib-jshint");
    grunt.loadNpmTasks("grunt-html-build");
    grunt.loadNpmTasks("grunt-browserify");

    //Speed Up grunt-sass (with node-sass)
    // const Fiber = require('fibers');
    const sass = require('node-sass');
    //End Speed Up grunt-sass (with node-sass)

    var serverStatic = require("serve-static");

    // Configurable paths for the application
    var appConfig = {
        app: ".",
        path: "/resources/views",
        js: "/public/js",
        css: "/public/css",
        images: "public/images",
        port: 3002
    };

    // Define the configuration for all the tasks
    grunt.initConfig({
        // Project settings
        appConfig: appConfig,

        // Watches files for changes and runs tasks based on the changed files
        watch: {
            // Whether to spawn task runs in a child process. Setting this option to false
            // speeds up the reaction time of the watch (usually 500ms faster for most)
            // and allows subsequent task runs to share the same context. Not spawning
            // task runs can make the watch more prone to failing so please use as needed.

            // options: { spawn: false },

            // End Speed Up Grunt (with Livereload on grunt-contrib-watch)

            //Speed Up Grunt (with Livereload)
            options: {nospawn: true, livereload: true},
            // End Speed Up Grunt (with Livereload)

            js: {
                files: ["<%= appConfig.app %>/js/**/{,*/}*.js"],
                tasks: ["newer:jshint:all", "browserify"],
                options: {
                    livereload: "<%= connect.options.livereload %>"
                }
            },
            styles: {
                files: ["<%= appConfig.app %>/sass/**/*.scss"],
                tasks: ["sass"],
                options: {
                    livereload: "<%= connect.options.livereload %>"
                }
            },
            gruntfile: {
                files: ["Gruntfile.js"]
            },
            html: {
                files: ["<%= appConfig.app %>/views/**/*.html"],
                tasks: ["htmlbuild"],
                options: {
                    livereload: "<%= connect.options.livereload %>"
                }
            },
            livereload: {
                options: {
                    livereload: "<%= connect.options.livereload %>"
                },
                files: [
                    "<%= appConfig.app %>/views/**/*.html",
                    "<%= appConfig.app %>/sass/**/*.scss",
                    "<%= appConfig.app %>/images/**/*.{png,jpg,jpeg,gif,webp}"
                ]
            }
        },

        // The actual grunt server settings
        connect: {
            options: {
                port: appConfig.port,
                // Change this to "0.0.0.0" to access the server from outside.
                hostname: "127.0.0.1",
                livereload: 35727
            },
            livereload: {
                options: {
                    open: {
                        target: "http://localhost:" + appConfig.port + appConfig.path
                    },
                    middleware: function (connect) {
                        return [serverStatic(appConfig.app)];
                    }
                }
            }
        },

        // Make sure code styles are up to par and there are no obvious mistakes
        jshint: {
            options: {
                reporter: require("jshint-stylish"),
                esversion: 6
            },
            all: {
                src: "<%= appConfig.app %>/js/**/*.js"
            }
        },

        browserify: {
            build: {
                src: "<%= appConfig.app %>/js/main.js",
                dest: "<%= appConfig.app %>/public/js/main.js"
            }
        },

        // compile sass files
        sass: {
            //Speed Up grunt-sass (with node-sass)
            // options: {
            //   implementation: sass,
            //   fiber: Fiber,
            //   sourceMap: true
            // },
            //End speed Up grunt-sass (with node-sass)
            options: {implementation: sass, sourceMap: true},
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= appConfig.app %>/sass",
                        src: ["**/*.scss"],
                        dest: "<%= appConfig.app %>/public/css",
                        ext: ".css"
                    }
                ]
            }
        },

        validation: {
            files: {
                src: ["<%= appConfig.app %>/views/**/*.html"]
            }
        },

        // Add vendor prefixed styles
        autoprefixer: {
            options: {
                browsers: ["last 10 version"],
                map: true
            },
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: "<%= appConfig.app %>/public/css/",
                        src: "*.css",
                        dest: "<%= appConfig.app %>/public/css/"
                    }
                ]
            }
        },

        htmlbuild: {
            dist: {
                src: "<%= appConfig.app %>/views/**/*.html",
                dest: "resources/",
                options: {
                    beautify: true,
                    relative: true,
                    basePath: true,
                    recursive: true,
                    sections: {
                        breadcrumbs: "<%= appConfig.app %>/views/breadcrumbs.html",
                        password_reissue: "<%= appConfig.app %>/views/password_reissue.html",
                        password_reissue_comp: "<%= appConfig.app %>/views/password_reissue_comp.html",
                        password_reminder: "<%= appConfig.app %>/views/password_reminder.html",
                        information_details: "<%= appConfig.app %>/views/information_details.html",
                        confirm: "<%= appConfig.app %>/views/confirm.html",
                        complete: "<%= appConfig.app %>/views/complete.html",
                        entry: "<%= appConfig.app %>/views/entry.html",
                        entry_register: "<%= appConfig.app %>/views/entry_register.html",
                        entry_notregistration: "<%= appConfig.app %>/views/entry_notregistration.html",
                        mypage_change_password_entry: "<%= appConfig.app %>/views/mypage_change_password_entry.html",
                        mypage_cancel_order_complete: "<%= appConfig.app %>/views/mypage_cancel_order_complete.html",
                        mypage_cancel_order_confirm: "<%= appConfig.app %>/views/mypage_cancel_order_confirm.html",
                        cart: "<%= appConfig.app %>/views/cart.html",
                        layout: {
                            main: {
                                head: "<%= appConfig.app %>/views/layout/main-head.html",
                                foot: "<%= appConfig.app %>/views/layout/main-foot.html"
                            },
                            header: "<%= appConfig.app %>/views/layout/header.html",
                            footer: "<%= appConfig.app %>/views/layout/footer.html"
                        },
                        home: {},
                        detail: {
                            top: "<%= appConfig.app %>/views/detail/detail-top.html",
                        },
                        login: {
                            top: "<%= appConfig.app %>/views/login/login-top.html",
                        },
                        logout: {
                            top: "<%= appConfig.app %>/views/logout/logout-top.html",
                        }
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks("grunt-w3c-html-validation");

    grunt.registerTask("server", "Compile then start a connect web server", function (target) {
        grunt.task.run(["sass", "browserify", "htmlbuild", "connect:livereload", "watch"]);
    });

    grunt.registerTask("build", "Compile then start a connect web server", function (target) {
        grunt.task.run(["sass", "autoprefixer", "browserify", "htmlbuild"]);
    });

    grunt.registerTask("test", "Compile then start a connect web server", function (target) {
        grunt.task.run(["validation", "jshint"]);
    });
};
