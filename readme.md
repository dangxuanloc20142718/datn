## Install application
```bash
composer install --no-dev
cp .env.example .env
php artisan key:generate
php artisan storage:link
```
## Cache
```bash
php artisan config:cache
php artisan route:cache
```
## Clear cache + restart queue
```bash
sh clear.sh
```
## Data sample
```bash
composer dump-autoload
php artisan db:seed
```
# config session secure cookie in env ( with https)
SESSION_SECURE_COOKIE=true