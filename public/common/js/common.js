$(document).ready(function () {
    $(function () {
        $(".tab_menu > li").each(function (i) {
            $(this).attr('class', 'tab_menu' + (i + 1));
        });
    });
    $(".tabArea > .tab_menu > li:first").addClass("select");
    $(".tab_menu > li").hover(function () {
        $(this).css("cursor", "pointer");
    }, function () {
        $(this).css("cursor", "default");
    });

    $(".tabArea > div").hide();
    $(".tabArea > div:first").show();
    $('.tabArea li a[href]').click(function (e) {
        e.preventDefault()
    });

    $(".tabMenuTop > li").click(function () {
        $(".tabMenuTop > li").removeClass("select");
        $(this).addClass("select");
        $(".tabArea > div").hide();
        $(".tabArea > div").eq($(".tabMenuTop > li").index(this)).show();
    });
    $('.form-sort').sortable({});
});

$(function () {
    $(".tabMenu > li").hover(function () {
            $(this).addClass('over');
        },
        function () {
            $(this).removeClass('over');
        }
    );
});

$(window).load(function () {
    var id = $(".tabNav li.active").attr('content');
    $("." + id).attr('style', 'display:block');
    $(".tabNav li").click(function () {
        $(".tabNav li").removeClass("active");
        $(this).addClass("active");
        var id = $(".tabNav li.active").attr('content');
        $(".border_box").hide();
        $("." + id).attr('style', 'display: block');
    });
});


$(window).load(function () {
    $(".tabMenu01 li:first").addClass("active");
    $(".tabBox01 > div").hide();
    $(".tabBox01 > div:first").show();

    $(".tabMenu01 li").click(function () {
        $(".tabMenu01 li").removeClass("active");
        $(this).addClass("active");
        $(".tabBox01 > div").hide();
        $(".tabBox01 > div").eq($(".tabMenu01 li").index(this)).show();
    });
});

$(window).load(function () {
    $(".tabMenu02 li:first").addClass("active");
    $(".tabBox02 > div").hide();
    $(".tabBox02 > div:first").show();

    $(".tabMenu02 li").click(function () {
        $(".tabMenu02 li").removeClass("active");
        $(this).addClass("active");
        $(".tabBox02 > div").hide();
        $(".tabBox02 > div").eq($(".tabMenu02 li").index(this)).show();
    });
});

//
// toggleMenu
//

$(function () {
    var over_flg = false;
    $("#hd .toggleMenuIco a").click(function () {
        $("#hd .toggleMenu").toggle();
        $("#ctAreaTop .toggleMenu").hide();
    });
    $("#hd .toggleMenuIco a").hover(function () {
        over_flg = true;
    }, function () {
        over_flg = false;
    });
    $('body').click(function () {
        if (over_flg == false) {
            $("#hd .toggleMenu").toggle(false);
        }
    });
});

$(function () {
    var over_flg = false;
    $("#ctAreaTop .toggleMenuIco a").click(function () {
        $("#ctAreaTop .toggleMenu").toggle();
        $("#hd .toggleMenu").hide();
    });
    $("#ctAreaTop .toggleMenuIco a").hover(function () {
        over_flg = true;
    }, function () {
        over_flg = false;
    });
    $('body').click(function () {
        if (over_flg == false) {
            $("#ctAreaTop .toggleMenu").toggle(false);
        }
    });
});

//
// JSでtarget=”_blank”みたいに別ウィンドウにする方法
// 使い方 Class.blankLinkを付けると自動的に target="_blank" を追加します。
//

$(function () {
    $("a.blankLink").attr("target", "_blank");
});

//
//div全体をリンクにする
//

$(function () {
    $(".link_box").click(function () {
        window.location = $(this).find("a").attr("href");
        return false;
    });
});

//
//スムーズスクロール
//

$(document).ready(function () {
    $('p.pagetop a[href*=#]').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
            && location.hostname == this.hostname) {
            var $target = $(this.hash);
            $target = $target.length && $target
                || $('[name=' + this.hash.slice(1) + ']');
            if ($target.length) {
                var targetOffset = $target.offset().top;
                $('html,body')
                    .animate({scrollTop: targetOffset}, 1000, 'easeOutExpo');
                return false;
            }
        }
    });
});


//
//ロールオーバー
//
;(function ($) {
    $(document).ready(function () {
        $("a img[src*='_ov']").addClass("current");
        $("a img[class!='current'],:image").mouseover(function () {
            if ($(this).attr("src").match(/_ot./)) {
                $(this).attr("src", $(this).attr("src").replace("_ot.", "_ov."));
                return;
            }
        }).mouseout(function () {
            if ($(this).attr("src").match(/_ov./)) {
                $(this).attr("src", $(this).attr("src").replace("_ov.", "_ot."));
                return;
            }
        }).click(function () {
            if ($(this).attr("src").match(/_ov./)) {
                $(this).attr("src", $(this).attr("src").replace("_ov.", "_ot."));
                return;
            }
        });


//preload images
        var images = [];
        $("a img,:image").each(function (index) {
            if ($(this).attr("src").match(/_ot./)) {
                images[index] = new Image();
                images[index].src = $(this).attr("src").replace("_ov.", "_ot.");
            }
        });
    });
})(jQuery);

//フォームテキストエリア内

function cText(obj) {
    if (obj.value == obj.defaultValue) {
        obj.value = "";
        obj.style.color = "#000";
    }
}

function sText(obj) {
    if (obj.value == "") {
        obj.value = obj.defaultValue;
        obj.style.color = "#999";
    }
}


//IEにCSS3を適用する 

$(function () {
    $('.addPie').each(function () {
        PIE.attach(this);
    });
});

function increase(url) {
    window.location.href = url;
}

function deincrease(url) {
    window.location.href = url;
}

function nowPage(url, pageNow, totalPage) {
    var post = url.lastIndexOf('page');
    var index = Number($('#page').val());
    if (index !== pageNow && index > 0 && index <= totalPage) {
        var urlPageNew = url.slice(0, post + 5) + index;
        window.location.href = urlPageNew;
    } else {
        $('#page').val(pageNow);
    }
}

function checkActiveOverview() {
    var url = window.location.href;
    var check = url.split('?')[1];
    if (check == 'overview' || check == 'overview&flag') {
        activeTabOverView()
    }
}

function activeTabOverView() {
    $('.nav1').removeClass('active');
    $('.nav2').addClass('active');
    $('.nav2').click();
}

function addRow() {
    var data_html = $("#row_overview").html();
    $(".form-sort").append(data_html);
}

function resetText() {
    $('input[type=password]').val('');
    $('input[type=text]').val('');
}

function backPage(url) {
    window.location.href = url;
}