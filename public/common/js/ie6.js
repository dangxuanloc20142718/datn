/**---------------------------------
 * ie6.js
---------------------------------------*/
 ;(function($){
		$(document).ready(function(){
			//png fix
			if(DD_belatedPNG){
				$("img[src$='png']").addClass("pngfix");
				$("*").each(function(index,elem){
					var $elem = $(elem);
					if($elem.css("background-image") && $elem.css("background-image").indexOf("png") > -1){
						$elem.addClass("pngfix");
					}
				})
				DD_belatedPNG.fix('.pngfix');
			}
		})
})(jQuery);
	
/**---------------------------------
 * ie6でもロールオーバが動く処理
---------------------------------------*/

var oldFixPng = DD_belatedPNG.fixPng;
DD_belatedPNG.fixPng = function (el) {
	oldFixPng(el);
	if (el.vml && el.vml.image.fill.getAttribute("src").match(/_ot\./)) {
					el.vml.image.shape.attachEvent('onmouseenter', function() {
									var image = el.vml.image.fill;
									image.setAttribute("src", image.getAttribute("src").replace("_ot.", "_ov."));
					});
					el.vml.image.shape.attachEvent('onmouseleave', function() {
									var image = el.vml.image.fill;
									image.setAttribute("src", image.getAttribute("src").replace("_ov.", "_ot."));
					});
	}
};