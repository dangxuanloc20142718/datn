/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	 //config.language = 'jp';
	// config.uiColor = '#AADC6E';
	config.language = 'ja';
	 //config.filebrowserBrowseUrl = '/common/ckfinder/ckfinder.html';
   //config.filebrowserImageBrowseUrl = '/common/ckfinder/ckfinder.html?type=Images';
   //config.filebrowserFlashBrowseUrl = '/common/ckfinder/ckfinder.html?type=Flash';
   //config.filebrowserUploadUrl = '/common/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files';
   //config.filebrowserImageUploadUrl = '/common/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';
   //config.filebrowserFlashUploadUrl = '/common/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';
};
